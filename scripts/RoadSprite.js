RoadSprite = function(roadYStart, roadHeight, roadMiddle, roadWidth)
{
	this.currentCenterIndex = 0;
	this.centerIndexMax = 30;
	this.width = roadWidth;
	this.slideIndex = 30;
	this.maxSlideIndex = 30;
	
	this.centerPoint = new Vector2(roadMiddle, roadYStart);
	this.basePointLeft = new Vector2(roadMiddle - (roadWidth / 2.0), roadYStart + roadHeight);
	this.basePointRight = new Vector2(roadMiddle + (roadWidth / 2.0), roadYStart + roadHeight);
	
	this.l_to_center_vec = new Vector2((this.centerPoint.x - this.basePointLeft.x), (this.centerPoint.y - this.basePointLeft.y));
	this.handle_max_length = Math.sqrt(this.l_to_center_vec.x * this.l_to_center_vec.x + this.l_to_center_vec.y * this.l_to_center_vec.y);
	this.l_to_center_vec.Normalize();
	this.r_to_center_vec = new Vector2((this.centerPoint.x - this.basePointRight.x), (this.centerPoint.y - this.basePointRight.y));
	this.r_to_center_vec.Normalize();
};

RoadSprite.prototype.Draw = function(canvas)
{
	canvas.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
	
	canvas.rect(0,CANVAS_HEIGHT/2, CANVAS_WIDTH, CANVAS_HEIGHT/2);
	canvas.fillStyle = "rgba(0,0,0,1)";
	canvas.fill();
	
	var handleLengths = ((this.maxSlideIndex - this.slideIndex) / this.maxSlideIndex) * this.handle_max_length;
	var mid_y = ((this.maxSlideIndex - this.slideIndex) / this.maxSlideIndex) * this.basePointLeft.y +
				(this.slideIndex / this.maxSlideIndex) * this.centerPoint.y;
	var mid_x_L = ((this.maxSlideIndex - this.slideIndex) / this.maxSlideIndex) * this.basePointLeft.x +
				(this.slideIndex / this.maxSlideIndex) * this.centerPoint.x;
	var mid_x_R = ((this.maxSlideIndex - this.slideIndex) / this.maxSlideIndex) * this.basePointRight.x +
				(this.slideIndex / this.maxSlideIndex) * this.centerPoint.x;
	var center_x = (this.currentCenterIndex/this.centerIndexMax) * (this.centerPoint.x - mid_x_L) + this.centerPoint.x;
	
	var mid_L_Handle = new Vector2(this.l_to_center_vec.x, this.l_to_center_vec.y);
	mid_L_Handle.multiplyComponents(handleLengths);
	mid_L_Handle.x += mid_x_L;
	mid_L_Handle.y += mid_y;
	var mid_R_Handle = new Vector2(this.r_to_center_vec.x, this.r_to_center_vec.y);
	mid_R_Handle.multiplyComponents(handleLengths);
	mid_R_Handle.x += mid_x_R;
	mid_R_Handle.y += mid_y;
	
	canvas.save();
	canvas.beginPath();
	//start making path begining at bottom left point
	canvas.moveTo(this.basePointLeft.x, this.basePointLeft.y);
	canvas.lineTo(mid_x_L, mid_y);
	
	//midpoint_L to center point
	canvas.bezierCurveTo(mid_L_Handle.x, mid_L_Handle.y, center_x, this.centerPoint.y, center_x, this.centerPoint.y);
	
	//center to midpoint_R
	canvas.bezierCurveTo(center_x, this.centerPoint.y, mid_R_Handle.x, mid_R_Handle.y, mid_x_R, mid_y);
	
	//midpoint to bottom right
	canvas.lineTo(this.basePointRight.x, this.basePointRight.y);
	
	//close path
	canvas.closePath();
	//draw
	
	canvas.clip();
	canvas.fillStyle = '#1f2033';
	canvas.fill();
	
	//ToDo: Center Line + approaching curve color change

	canvas.restore();
};

RoadSprite.prototype.resize = function(roadYStart, roadHeight, roadMiddle, roadWidth)
{
	this.centerPoint = new Vector2(roadMiddle, roadYStart);
	this.basePointLeft = new Vector2(roadMiddle - (roadWidth / 2.0), roadYStart + roadHeight);
	this.basePointRight = new Vector2(roadMiddle + (roadWidth / 2.0), roadYStart + roadHeight);
	
	this.l_to_center_vec = new Vector2((this.centerPoint.x - this.basePointLeft.x), (this.centerPoint.y - this.basePointLeft.y));
	this.handle_max_length = Math.sqrt(this.l_to_center_vec.x * this.l_to_center_vec.x + this.l_to_center_vec.y * this.l_to_center_vec.y);
	this.l_to_center_vec.Normalize();
	this.r_to_center_vec = new Vector2((this.centerPoint.x - this.basePointRight.x), (this.centerPoint.y - this.basePointRight.y));
	this.r_to_center_vec.Normalize();
}