Vector2 = function(x, y)
{
	this.x = x;
	this.y = y;
};

Vector2.prototype.multiplyComponents = function(mag)
{
	this.x = this.x * mag;
	this.y = this.y * mag;
};

Vector2.prototype.Normalize = function()
{
	var hyp = Math.sqrt(this.x * this.x + this.y * this.y);
	this.x = (this.x / hyp);
	this.y = (this.y / hyp);
};

Vector2.prototype.Rotate = function(radians)
{
	this.x = Math.cos(radians) * this.x - Math.sin(radians) * this.y;
	this.y = Math.sin(radians) * this.x + Math.cos(radians) * this.y;
};

Vector2.prototype.AngleBetween = function(vec)
{
	var numer = this.x * vec.x + this.y * vec.y;
	var denom = Math.sqrt(this.x * this.x + this.y * this.y) * Math.sqrt(vec.x * vec.x + vec.y * vec.y);
	
	var theta = numer / denom;
	
	return Math.cos(theta);
};