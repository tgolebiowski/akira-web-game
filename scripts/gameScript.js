var CANVAS_WIDTH = window.innerWidth;
var CANVAS_HEIGHT = window.innerHeight;
var FPS = 30;
var scaling = (CANVAS_HEIGHT / 900.0);
var offset = offset = new Vector2(((1500.0 * scaling)- CANVAS_WIDTH) * 0.5, ((900.0 * scaling) - CANVAS_HEIGHT) * 0.5);

var fadeInIndex = 0;
var maxFadeIn = 60;

var redrawBackground = true;
var player = new Player(scaling, CANVAS_WIDTH);

var bgcanvas;
var levelCanvas;
var imageCanvas;
var playerCanvas;
var fxCanvas;

var gameover = false;

var explosionsrcs = ['images/Exp1.png', 'images/Exp2.png', 'images/Exp3.png', 'images/Exp4.png', 'images/Exp5.png', 'images/Exp6.png', 'images/Exp7.png', 'images/Exp8.png', 'images/Exp9.png', 'images/Exp10.png', 'images/Exp11.png', 'images/Exp12.png', 'images/Exp13.png'];
var explosionImages = new Array();
var explosionTimer = 0;

var distanceCounter = 0;

Game = function()
{
	var canvasElementBG = $("<canvas width='" + CANVAS_WIDTH + "' height='" + CANVAS_HEIGHT +  "'style='position: absolute; z-index=0'></canvas");
	bgcanvas = canvasElementBG.get(0).getContext("2d");
	var canvasElementFG = $("<canvas width='" + CANVAS_WIDTH + "' height='" + CANVAS_HEIGHT +  "'style='position: absolute; z-index=1'></canvas");
	levelCanvas = canvasElementFG.get(0).getContext("2d");
	var canvasElementImages = $("<canvas width='" + CANVAS_WIDTH + "' height='" + CANVAS_HEIGHT +  "'style='position: absolute; z-index=2'></canvas");
	imageCanvas = canvasElementImages.get(0).getContext("2d");
	var canvasElementPlayer = $("<canvas width='" + CANVAS_WIDTH + "' height='" + CANVAS_HEIGHT +  "'style='position: absolute; z-index=3'></canvas");
	playerCanvas = canvasElementImages.get(0).getContext("2d");
	var canvasElementFX = $("<canvas width='" + CANVAS_WIDTH + "' height='" + CANVAS_HEIGHT +  "'style='position: absolute; z-index=4'></canvas");
	fxCanvas = canvasElementFX.get(0).getContext("2d");
	
	canvasElementBG.appendTo('body');
	canvasElementFG.appendTo('body');
	canvasElementImages.appendTo('body');
	canvasElementPlayer.appendTo('body');
	canvasElementFX.appendTo('body');
	
	for(var i = 0; i < explosionsrcs.length; i++)
	{
		explosionImages.push(new Image);
		explosionImages[i].src = explosionsrcs[i];
	}
	
	window.addEventListener("resize", this.resizeScript, false);
	
	this.bg = new StaticImage('images/CityBackground.png');
	this.roadSprite = new RoadSprite(CANVAS_HEIGHT/2, CANVAS_HEIGHT/2, CANVAS_WIDTH/2, CANVAS_WIDTH);
	this.gameplayMGMT = new GameplayManager(this.roadSprite, player);
	
	redrawBackground = true;
};

Game.prototype.Update = function()
{
	fadeInIndex++;
	if(fadeInIndex >= maxFadeIn) fadeInIndex = maxFadeIn;
	if(!gameover)
	{
		player.Update();
		this.gameplayMGMT.Update();
		distanceCounter++;
	}
	else
	{
		
	}
};

Game.prototype.Draw = function()
{
	if(redrawBackground)
	{
		bgcanvas.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
		bgcanvas.beginPath();
		bgcanvas.rect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
		bgcanvas.fillStyle = '#231139';
		bgcanvas.fill();
		var goodDraw = this.bg.Draw(bgcanvas, -offset.x, 0, scaling);
		redrawBackground = !goodDraw;
	}
	
	if(!gameover)
	{
		player.Draw(playerCanvas);
	}
	else
	{
		playerCanvas.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
		if(explosionTimer < explosionImages.length)
		{
			playerCanvas.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
			playerCanvas.drawImage(explosionImages[explosionTimer], -(explosionImages[explosionTimer].naturalWidth/2) + playerCurrentX, -(explosionImages[		explosionTimer].naturalHeight / 2) + yPosition, explosionImages[explosionTimer].naturalWidth * scaling, explosionImages[explosionTimer].		naturalHeight * scaling);
			explosionTimer++;
		}
	}
	
	fxCanvas.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
	if(fadeInIndex < maxFadeIn)
	{
		fxCanvas.rect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
		fxCanvas.fillStyle = "rgba(0,0,0," + ((maxFadeIn - fadeInIndex)/maxFadeIn) + ")";
		fxCanvas.fill();
	}
	fxCanvas.font = "30 px Helvetica";
	fxCanvas.fillStyle = '#ffffff';
	fxCanvas.fillText("Meters Traveled: " + distanceCounter, CANVAS_WIDTH / 2 - 100, 25);
	
	if(gameover)
	{
		fxCanvas.font = "20 px Helvetica";
		fxCanvas.fillText("Game Over, Reload To Restart", CANVAS_WIDTH/2 - 150, CANVAS_HEIGHT/2 + 200);
	}
	
	this.roadSprite.Draw(levelCanvas);
};

Game.prototype.resizeScript = function()
{
	CANVAS_WIDTH = window.innerWidth;
	CANVAS_HEIGHT = window.innerHeight;
	
	bgcanvas.width = CANVAS_WIDTH;
	bgcanvas.height = CANVAS_HEIGHT;
	levelCanvas.width = CANVAS_WIDTH;
	levelCanvas.height = CANVAS_HEIGHT;
	playerCanvas.width = CANVAS_WIDTH;
	playerCanvas.height = CANVAS_HEIGHT;
	fxCanvas.width = CANVAS_WIDTH;
	fxCanvas.height = CANVAS_HEIGHT;
	imageCanvas.width = CANVAS_WIDTH;
	imageCanvas.height = CANVAS_HEIGHT;
	
	levelCanvas.clearRect(0,0,CANVAS_WIDTH, CANVAS_HEIGHT);
	
	scaling = (CANVAS_HEIGHT / 900.0);
	player.resize(scaling);
	
	offset = new Vector2(((1500.0 * scaling)- CANVAS_WIDTH) * 0.5, ((900.0 * scaling) - CANVAS_HEIGHT) * 0.5);
	redrawBackground = true;
	
	this.roadSprite.resize(CANVAS_HEIGHT/2, CANVAS_HEIGHT/2, CANVAS_WIDTH/2, CANVAS_WIDTH);
};

