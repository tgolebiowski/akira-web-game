var maxMoveSpeed = 15;

GameplayManager = function(roadSprite, player)
{
	this.road = roadSprite;
	this.player = player;
	
	this.targetCurvePosition = 0;
	this.ticsTillNextCurve = 0;
	this.curveleft = 0;
};

GameplayManager.prototype.Update = function()
{
	var targetCurrentDiff = this.targetCurvePosition - this.road.currentCenterIndex;
	if(targetCurrentDiff != 0)
	{
		var currentChange = Math.sign(targetCurrentDiff);
		this.road.currentCenterIndex += currentChange;
	}
	
	if(this.road.slideIndex > 0 && this.targetCurvePosition != 0)
	{
		this.road.slideIndex--;
	}
	
	if(this.road.slideIndex == 0)
	{
		var curvePush = maxMoveSpeed * (-this.road.currentCenterIndex / this.road.centerIndexMax);
		playerCurrentX += curvePush;
	}
	
	this.curveLeft--;
	if(this.curveLeft <= 0)
	{
		this.targetCurvePosition = 0;
		if(this.road.currentCenterIndex == 0)
		{
			this.road.slideIndex = this.road.maxSlideIndex;
		}
	}
	
	this.ticsTillNextCurve--;
	if(this.ticsTillNextCurve <= 0)
	{
		this.newCurve();
	}
};

GameplayManager.prototype.newCurve = function()
{
	this.ticsTillNextCurve = Math.floor(Math.random() * 60) + 60;
	this.curveLeft = Math.floor(this.ticsTillNextCurve * (3/4));
	this.targetCurvePosition = Math.floor(Math.random() * 60) - 30;
};
