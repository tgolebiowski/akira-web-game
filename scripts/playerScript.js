var playerCurrentX;
var playerLeanIndex = 0;
var maxLean = 11;
var minX;
var maxX;
var yPosition;

var light1X;
var light1Y;
var light2X;
var light2Y;
var light1XQueue = [];
var light1YQueue = [];
var light2XQueue = [];
var light2YQueue = [];
var queueMax = 24;
		
var light1BaseTheta = (Math.PI / 2.0) + (Math.PI / 32.0);
var light2BaseTheta = (Math.PI / 2.0) - (Math.PI / 32.0);
var lightsRadius = 59;
		
var light1BaseTheta = (Math.PI / 2.0) + (Math.PI / 32.0);
var light2BaseTheta = (Math.PI / 2.0) - (Math.PI / 32.0);
var lightsRadius = 59;

Player = function(scaling, CANVAS_WIDTH)
{
	this.sprite = new Image();
	this.sprite.src = 'images/Bike.png';
	
	playerCurrentX = CANVAS_WIDTH / 2;
	minX = CANVAS_WIDTH / 4.0;
	maxX = CANVAS_WIDTH * 3 / 4.0;
	
	this.playerTargetHeight = 165.0 * scaling;
	lightsRadius = lightsRadius * scaling;
	yPosition = CANVAS_HEIGHT - ((3/4) * (this.playerTargetHeight));
	
	light1X = playerCurrentX + Math.cos(light1BaseTheta) * lightsRadius;
	light1Y = yPosition + Math.sin(light1BaseTheta) * lightsRadius;
	light2X = playerCurrentX + Math.cos(light2BaseTheta) * lightsRadius + 2;
	light2Y = yPosition + Math.sin(light2BaseTheta) * lightsRadius;
		
	for(var i = 0; i < queueMax; i++)
	{
		light1XQueue.push(light1X);
		light1YQueue.push(light1Y);
		light2XQueue.push(light2X);
		light2YQueue.push(light2Y);
	}
};

Player.prototype.resize = function(scaling)
{
	playerCurrentX = CANVAS_WIDTH / 2;
	minX = CANVAS_WIDTH / 4.0;
	maxX = CANVAS_WIDTH * 3 / 4.0;
	
	this.playerTargetHeight = 165.0 * scaling;
	lightsRadius = lightsRadius * scaling;
	yPosition = CANVAS_HEIGHT - ((3/4) * (this.playerTargetHeight));
};

Player.prototype.Draw = function(playerCanvas)
{
	playerCanvas.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
	playerCanvas.save();
	var rotationThisFrame = (Math.PI / (4 * maxLean)) * playerLeanIndex;
	playerCanvas.translate(playerCurrentX , yPosition);
	playerCanvas.rotate(rotationThisFrame);
	playerCanvas.drawImage(this.sprite, -(this.playerTargetHeight / 2.0), -(this.playerTargetHeight * (3/4)), this.playerTargetHeight, this.playerTargetHeight);
	playerCanvas.restore();
	
	this.drawBikeLights(playerCanvas);
};

Player.prototype.drawBikeLights = function(canvas)
{
	canvas.save();
		
	var lightsStart = "rgba(10, 237, 144, 1.0)";
	var lightsEnd = "rgba(8, 37, 48, 0.0)";
	var grd = canvas.createLinearGradient(playerCurrentX, yPosition, playerCurrentX, CANVAS_HEIGHT);
	grd.addColorStop(0, lightsStart);
	grd.addColorStop(1, lightsEnd);
			
	canvas.strokeStyle=grd;
	canvas.lineCap='round';
	canvas.lineWidth = 5;
	canvas.beginPath();
	canvas.moveTo(light1XQueue[0], light1YQueue[0]);
	for(var i = 1; i < queueMax; i++)
	{
		var light1XThisTic = light1XQueue[i];
		var light1YThisTic = light1YQueue[i] + (i * 8.25);
				
		canvas.lineTo(light1XThisTic, light1YThisTic);
	}
	canvas.stroke();
			
	canvas.beginPath();
	canvas.moveTo(light2XQueue[0], light2YQueue[0]);
	for(var i = 1; i < queueMax; i++)
	{
		var light2XThisTic = light2XQueue[i];
		var light2YThisTic = light2YQueue[i] + (i * 8.25);
		
		canvas.lineTo(light2XThisTic, light2YThisTic);
	}
	canvas.stroke();
			
	canvas.restore();
}

Player.prototype.Update = function()
{
	if(keydown.left && keydown.right)
	{
		//do nothing, do not adjust the index, lock them in to the current lean.
	}
	else if(keydown.left) 
	{
		playerLeanIndex--;
		if(Math.abs(playerLeanIndex) > maxLean) playerLeanIndex = -maxLean;
	}
	else if(keydown.right) 
	{
		playerLeanIndex++;
		if(Math.abs(playerLeanIndex) > maxLean) playerLeanIndex = maxLean;
	}
	else
	{
		if(playerLeanIndex > 0) playerLeanIndex--;
		if(playerLeanIndex < 0) playerLeanIndex++;
	}
		
	playerCurrentX = playerCurrentX + (15 * (playerLeanIndex / maxLean));	
		
	if(playerCurrentX < minX) gameover = true;
	if(playerCurrentX > maxX) gameover = true;
		
	var signChange = 1;
	if(playerLeanIndex < 0) signChange = -1;
			
	var rotationThisFrame = (Math.PI / (4 * maxLean)) * playerLeanIndex;
	var light1Rotation = rotationThisFrame - light1BaseTheta;
	var light2Rotation = rotationThisFrame - light2BaseTheta;
	light1X = playerCurrentX  + Math.cos(light1Rotation) * lightsRadius;
	light1Y = yPosition + Math.sin(light1Rotation) * lightsRadius;
	light2X = playerCurrentX  + Math.cos(light2Rotation) * lightsRadius + 2;
	light2Y = yPosition + Math.sin(light2Rotation) * lightsRadius;

	for(var i = queueMax - 1; i >= 1; i--)
	{
		light1XQueue[i] = light1XQueue[i - 1];
		light1YQueue[i] = light1YQueue[i - 1];
		light2XQueue[i] = light2XQueue[i - 1];
		light2YQueue[i] = light2YQueue[i - 1];
	}
			
	light1XQueue[0] = light1X;
	light1YQueue[0] = light1Y;
	light2XQueue[0] = light2X;
	light2YQueue[0] = light2Y;
};