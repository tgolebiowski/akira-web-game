
StaticImage = function(imageSrc)
{
	this.sprite = new Image();
	this.sprite.src = imageSrc;
};

StaticImage.prototype.Draw = function(canvas, x, y, scaling)
{
	canvas.drawImage(this.sprite, x, y, this.sprite.naturalWidth * scaling, this.sprite.naturalHeight * scaling);
};

DrawnSection = function(width, height, x, y)
{
	this.width = width;
	this.height = height;
	this.x = x;
	this.y = y;
	
	return this.isLoaded;
};